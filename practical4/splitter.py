import sys

MD5 = '1'
ARGON2I = 'argon2i'
SHA256 = '5'
SHA512 = '6'
PBKDF22 = 'pbkdf2-sha256'

def split(file):
  linesmd5 = []
  linesargon2i = []
  linessha256 = []
  linessha512 = []
  linesmpbkdf2 = []
  linesdes = []

  f = open(file, "r")
  count = 1
  for line in f:
    #print(str(count) + ' ' + line) 
    count += 1
    if line[0] == '$':
      hf = line[1:line.find("$", 1)]
      if hf == MD5:
        linesmd5.append(line)
      elif hf == ARGON2I:
        linesargon2i.append(line)
      elif hf == SHA256:
        linessha256.append(line)
      elif hf == SHA512:
        linessha512.append(line)
      elif hf == PBKDF22:
        linesmpbkdf2.append(line)
    else:
      linesdes.append(line)
    writeToFile('panezveg.hashes.md5', linesmd5)
    writeToFile('panezveg.hashes.argon2i', linesargon2i)
    writeToFile('panezveg.hashes.sha256', linessha256)
    writeToFile('panezveg.hashes.sha512', linessha512)
    writeToFile('panezveg.hashes.pbkdf2', linesmpbkdf2)
    writeToFile('panezveg.hashes.des', linesdes)
  return 0

def writeToFile(name, lines):
  with open(name, 'w') as f:
    for line in lines:
      f.write(line)
    f.close()

def main():
  filePath = sys.argv[1]
  print("arg:",filePath)
  split(filePath)

if __name__ == '__main__':
    main()
